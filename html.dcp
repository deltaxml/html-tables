<?xml version="1.0" encoding="UTF-8"?>
<documentComparator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                    xsi:noNamespaceSchemaLocation="core-dcp-v1_0-schema1_1.xsd" 
                    version="1.0" 
                    id="html" 
                    description="[DCP] XML Compare HTML Tables">

  <fullDescription> Produce an HTML or Oxygen Track Changes output from DITA inputs with HTML tables. </fullDescription>

  <pipelineParameters>
    <booleanParameter name="oxygen-output" defaultValue="false">
      <description>Set output to be Oxygen Track Change format instead of HTML default.</description>
    </booleanParameter>
    <booleanParameter name="include-css" defaultValue="true">
      <description>For HTML output include the CSS styles in the HTML instead of linking to the CSS file.</description>
    </booleanParameter>
  </pipelineParameters>

  <!-- Hardcode Oxygen Track Changes -->
  <standardConfig>
    <tableConfiguration>
      <processHtmlTables literalValue="true"/>
    </tableConfiguration>
    <outputFormatConfiguration>
      <resultFormat xpath="if ($oxygen-output) then 'oxygen-tc' else 'delta'"/>
      <modifiedAttributeMode literalValue="change"/>
    </outputFormatConfiguration>
    <resultReadabilityOptions>
      <modifiedWhitespaceBehaviour literalValue="normalize"/>
      <mixedContentDetectionScope literalValue="local"/>
    </resultReadabilityOptions>
    <lexicalPreservation>
      <defaults>
        <retain literalValue="true"/>
        <outputType literalValue="useDefault"/>
        <processingMode literalValue="useDefault"/>
      </defaults>
      <overrides>
        <preserveItems>
          <contentModel>
            <retain literalValue="false"/>
          </contentModel>
          <ignorableWhitespace>
            <retain literalValue="false"/>
          </ignorableWhitespace>
          <documentLocation>
            <retain literalValue="false"/>
          </documentLocation>
          <XMLDeclaration>
            <retain literalValue="false"/>
          </XMLDeclaration>
          <doctype>
            <retain xpath="if ($oxygen-output) then 'true' else 'false'"/>
          </doctype>
        </preserveItems>
      </overrides>
    </lexicalPreservation>

  </standardConfig>

  <advancedConfig>
    <parserFeatures>
      <feature name="http://apache.org/xml/features/nonvalidating/load-external-dtd" literalValue="false"/>
    </parserFeatures>
  </advancedConfig>

  <extensionPoints>
    <outputExtensionPoints>
      <finalPoint>
        <filter when="not($oxygen-output)">
          <file path="style.xsl" relBase="dxp"/>
          <parameter name="include-css" xpath="xs:boolean($include-css)"/>
        </filter>
      </finalPoint>
    </outputExtensionPoints>
  </extensionPoints>

</documentComparator>
