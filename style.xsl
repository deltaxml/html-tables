<?xml version="1.0" encoding="UTF-8"?>
<!-- 
       Converts DeltaV2 output specifically to HTML tables with differences highlighted.
       This XSLT is not intended for any input files, just those included in the HTML table processing sample.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:preserve="http://www.deltaxml.com/ns/preserve"
  exclude-result-prefixes="#all"
  version="3.0">
  
  <xsl:mode on-no-match="shallow-copy"/>
  <xsl:output method="html" version="5"/>
  
  <xsl:param name="include-css" as="xs:boolean" select="true()"/>
  
  <xsl:template match="/">
    <html>
      <head>
        <xsl:choose>
          <xsl:when test="$include-css">
            <style>
              <xsl:sequence select="unparsed-text(resolve-uri('htmltable.css', static-base-uri()))"/>
            </style>
          </xsl:when>
          <xsl:otherwise>
            <link rel="stylesheet" type="text/css" href="htmltable.css"/>
          </xsl:otherwise>
        </xsl:choose>
        
        <xsl:apply-templates select="*:html/*:head/*:title"/>
      </head>
      <xsl:apply-templates select="*:html/*:body"/>
    </html>
  </xsl:template>
  
  <xsl:template match="deltaxml:textGroup">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="deltaxml:text">
    <span>
      <xsl:apply-templates select="@*, node()"/>
    </span>
  </xsl:template>
  
  <xsl:template match="*:table/*:title">
    <caption>
      <xsl:apply-templates/>
    </caption>
  </xsl:template>
  
  <xsl:template match="*:p[*:table]">
    <section>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  
  <xsl:template match="@deltaxml:deltaV2">
    <xsl:attribute name="data-deltaV2" select="."/>
  </xsl:template>
  
  <xsl:template match="@colname|@frame|@rowsep|@colsep"/>
  
  <xsl:template match="*[namespace-uri()] except preserve:comment">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="node() | @*"/>
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>